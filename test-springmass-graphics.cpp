/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{
private:
  Figure figure ;

public:
  SpringMassDrawable()
  : figure("SpringMass Test")
  {
    figure.addDrawable(this) ;
  }

  void draw() {
  	int i;
  	double thickness=1;
  	for(i=0;i<springs.size();i++){
  		figure.drawCircle(springs[i].getMass1()->getPosition().x,springs[i].getMass1()->getPosition().y,springs[i].getMass1()->getRadius()) ;
  		figure.drawCircle(springs[i].getMass2()->getPosition().x,springs[i].getMass2()->getPosition().y,springs[i].getMass2()->getRadius()) ;
  		figure.drawLine(springs[i].getMass1()->getPosition().x,springs[i].getMass1()->getPosition().y,springs[i].getMass2()->getPosition().x,springs[i].getMass2()->getPosition().y, thickness);
  	}
  }

  void display() {
    figure.update() ;
  }
/* INCOMPLETE: TYPE YOUR CODE HERE */

} ;

int main(int argc, char** argv)
{
  glutInit(&argc,argv) ;

  SpringMassDrawable springmass ;
  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 1 ;
  const double stiffness=1;
  const double damping=100;

  Mass m1(Vector2(-.5,0), Vector2(), mass, radius) ;
  Mass m2(Vector2(+.5,0), Vector2(), mass, radius) ;
  springmass.addMass(m1);
  springmass.addMass(m2);
  springmass.addSpring(0,1,naturalLength, stiffness, damping);


/* INCOMPLETE: TYPE YOUR CODE HERE */

  run(&springmass, 1/120.0) ;
}
