/** file: test-srpingmass.cpp
 ** brief: Tests the spring mass simulation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"

int main(int argc, char** argv)
{
  SpringMass springmass ;

  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.98 ;
  const double stiffness=1;
  const double damping=0.1;

  Mass m1(Vector2(-.5,0), Vector2(), mass, radius) ;
  Mass m2(Vector2(+.5,0), Vector2(), mass, radius) ;
  springmass.addMass(m1);
  springmass.addMass(m2);
  springmass.addSpring(0,1,naturalLength, stiffness, damping);
  /* DONE: TYPE YOUR CODE HERE 
     1. Adicione as duas massas instanciadas acima (2 linhas).
     2. Adicione uma mola com as duas massas (indexadas por 0 e 1),
        e com o parametro de comprimento em repouso inicializado acima.
	(1 linha).
   */

  const double dt = 1.0/100;//Diminuindo o dt (aumentando o denominador), tornou-se mais proximo do esperado
  for (int i = 0 ; i < 1000; ++i) {
    springmass.step(dt) ;
    springmass.display() ;
  }

  return 0 ;
}
