# Introdução

 > O projeto visa simular o comportamento de uma bola solta em uma caixa com determinadas dimensões e sob determinadas circunstância.
 
# Pré-requisitos

 > Para a execução do programa principal faz-se necessário a instalação dos pacotes freeglut3 e freeglut3-dev que podem ser obtidos através dos comandos:
 ```sh
 $sudo apt-get install freeglut3 freeglu3-dev
 ```
 > Para a plotagem do gráfico do gráfico foi utilizada a biblioteca matplotlib do python3 que pode ser obtida através do comando:
 ```sh
 $sudo apt-get install python3-matplotlib
 ```
 
# Instalação
 
 > Para a instação basta usar o comando make para gerar o executável:
 ```sh
 $make test-ball #Irá gerar um programa que imprime somente as coordenadas da bola a cada iteração
 $make test-ball-graphics #Irá gerar um programa com interface gráfica que imprime as coordenadas da bola
 $make test-springmass #Irá gerar um programa que imprime somente as coordenadas de ambas massas
 ```
 
# Execução e plotagem do gráfico do test-ball

 > Para rodar o make do test-ball e gerar o arquivo com as coordenadas para a plotagem, basta executar o comando:
 ```sh
 $make test-ball
 $./test-ball > teste.txt
 ```
 
 > Com isso, a saída será canalizada para um arquivo texto chamado teste e irá ser formado por duas colunas, sendo a primeira o valor da bola em x e a segunda, o valor em y. Utilizando o script [teste.py](teste.py) (em Python3) ou o [plot_ball.m](plot_ball.m) (em Octave ou Matlab) irá plotar o gráfico utilizando o arquivo de saída após a execução do programa e espera-se que se obtenha algo semelhante à figura:
 
 ![PlotGrafico](figure_1.png)
 
 >Para conferir, pode-se utilizar o make com o argumento test-ball-graphics e para isso basta usar o comando:
 ```sh
 $make test-ball-graphics
 $./test-ball-graphics
 ```
 > Então será executada uma janela mostrando, graficamente, a posição da bola.
 
# Execução e plotagem do gráfico do test-springmass

 >Para rodar o make do test-springmass e gerar o arquivo com as coordenadas (sendo X1 Y1 X2 Y2), basta executar o comando:
 ```sh
 $make test-springmass
 $.test-springmass > teste.txt
 ```
 >E para plotar o gráfico utilizando o arquivo de saída gerado pelo pip basta executar o script [teste-springmass.py](teste-springmass.py) através do python3 no terminal:
 ```sh
 $python3 teste-springmass.py
 ```
 